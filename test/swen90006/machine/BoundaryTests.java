package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
	//Any method annotated with "@Before" will be executed before each test,
	//allowing the tester to set up some shared resources.
	@Before public void setUp()
	{
	}

	//Any method annotated with "@After" will be executed after each test,
	//allowing the tester to release any shared resources used in the setup.
	@After public void tearDown()
	{
	}

	//Any method annotation with "@Test" is executed as a test.
	@Test public void aTest()
	{
		//the assertEquals method used to check whether two values are
		//equal, using the equals method
		final int expected = 2;
		final int actual = 1 + 1;
		assertEquals(expected, actual);
	}

	@Test public void anotherTest()
	{
		List<String> list = new ArrayList<String>();
		list.add("a");
		list.add("b");

		//the assertTrue method is used to check whether something holds.
		assertTrue(list.contains("a"));
	}

	//Test test opens a file and executes the machine
	@Test public void aFileOpenTest()
	{
		final List<String> lines = readInstructions("examples\\array.s");
		Machine m = new Machine();
		assertEquals(m.execute(lines), 45);
		assertEquals(m.getCount(),162);
	}

	@Test public void validAdd()
	{
		final List<String> lines = readInstructions("examples\\div_test2.s");
		Machine m = new Machine();
		assertEquals(m.execute(lines), 20);
	}

	//To test an exception, specify the expected exception after the @Test
	@Test(expected = java.io.IOException.class) 
	public void anExceptionTest()
			throws Throwable
	{
		throw new java.io.IOException();
	}

//	//This test should fail.
//	//To provide additional feedback when a test fails, an error message
//	//can be included
//	@Test public void aFailedTest()
//	{
//		//include a message for better feedback
//		final int expected = 2;
//		final int actual = 1 + 2;
//		assertEquals("Some failure message", expected, actual);
//	}

	// Equivalance Class 1
	@Test(expected = InvalidInstructionException.class)
	public void partitionofInstructionsInvalid() {

		// Invalid Instructions for MVO not in set of  ADD,SUB...JMP,JZ
		final List<String> lines = readInstructions("examples\\invalidInstructions.s");
		Machine m = new Machine();
		m.execute(lines);
	}

	@Test(expected = InvalidInstructionException.class)
	public void registersNotInRange() {

		// Register not in range of 0...31 -> selected value 33
		final List<String> lines = readInstructions("examples\\registerNotInRage.s");
		Machine m = new Machine();
		m.execute(lines);
	}
	
	@Test(expected = InvalidInstructionException.class)
	public void registersNotInRange2() {

		// Register not in range of 0...31 -> selected value -2
		final List<String> lines = readInstructions("examples\\registerNotInRage2.s");
		Machine m = new Machine();
		m.execute(lines);
	}

	@Test(expected = InvalidInstructionException.class)
	public void valuesNotInRange() {

		// value not in range of -65536...65535  -> 70,000
		final List<String> lines = readInstructions("examples\\valueNotInRage.s");
		Machine m = new Machine();
		m.execute(lines);
	}
	
	@Test(expected = InvalidInstructionException.class)
	public void valuesNotInRange2() {

		// value not in range of -65536...65535  -> -70,000
		final List<String> lines = readInstructions("examples\\valueNotInRage2.s");
		Machine m = new Machine();
		m.execute(lines);
	}

	@Test(expected = InvalidInstructionException.class)
	public void invalidInstructionlength() {

		// Invalid Instruction length, less than original length
		/*
		 * MOV R0 -> valid
		 * MOV    -> selected value
		 */
		final List<String> lines = readInstructions("examples\\invalidInstrctuinLength.s");
		Machine m = new Machine();
		m.execute(lines);
	}
	
	@Test(expected = InvalidInstructionException.class)
	public void invalidInstructionlength2() {

		// Invalid Instruction length, Greater than original length
		/*
		 * MOV R0 -> valid
		 * MOV R0 10 10 -> selected value
		 */
		final List<String> lines = readInstructions("examples\\invalidInstrctuinLength2.s");
		Machine m = new Machine();
		m.execute(lines);
	}

	@Test
	public void programHaltsAtError() {

		// pc does not move forward
		final List<String> lines = readInstructions("examples\\invalidInstrctuinLength.s");
		Machine m = new Machine();
		try { 

			m.execute(lines);
		}catch(InvalidInstructionException e) {
			// do nothing
		}

		assertTrue(m.getCount() < lines.size());
	}


	@Test public void MultipuleRuternTest() {

		// Multiple return statements
		final List<String> lines = readInstructions("examples\\multiReturn.s");
		Machine m = new Machine();
		int val = m.execute(lines);

		assertEquals(val, 0);
	}

	@Test(expected = NoReturnValueException.class)
	public void noReturnTest() {

		// Multiple return statements
		final List<String> lines = readInstructions("examples\\noReturn.s");
		Machine m = new Machine();
		m.execute(lines);

	}

	@Test(expected = NoReturnValueException.class)
	public void invalidJMPTest() {

		// Multiple return statements
		final List<String> lines = readInstructions("examples\\invalidJMP.s");
		Machine m = new Machine();
		m.execute(lines);

	}
	
	@Test
	public void validJMP1Test() {

		// Multiple return statements
		final List<String> lines = readInstructions("examples\\validJMP1.s");
		Machine m = new Machine();
		m.execute(lines);
		assertTrue(m.getCount() == lines.size());
	}
	

	@Test(expected = NoReturnValueException.class)
	public void invalidJZTest() {

		// Multiple return statements
		final List<String> lines = readInstructions("examples\\invalidJZ.s");
		Machine m = new Machine();
		m.execute(lines);

	}


	@Test public void NotExcuteWrongInstructions() {
		
		final List<String> lines = readInstructions("examples\\div_test.s");
		Machine m = new Machine();
		int val = m.execute(lines);

		assertEquals(val, 100);
	}

	//Read in a file containing a program and convert into a list of
	//string instructions
	private List<String> readInstructions(String file)
	{
		Charset charset = Charset.forName("UTF-8");
		List<String> lines = null;
		try {
			lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
		}
		catch (Exception e){
			System.err.println("Invalid input file! (stacktrace follows)");
			e.printStackTrace(System.err);
			System.exit(1);
		}
		return lines;
	}
}
