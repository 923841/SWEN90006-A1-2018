          ;; JMP with jumping with invalid register value
        MOV R3 -100             
        MOV R2 10               
		MOV R4 100

 		DIV R4 R3 R2
 		JMP -10000
 		RET R4